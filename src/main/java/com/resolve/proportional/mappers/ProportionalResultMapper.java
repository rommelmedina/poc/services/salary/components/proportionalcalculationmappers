/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.mappers;

import com.resolve.football.models.PlayerDto;
import com.resolve.football.models.TeamDto;
import com.resolve.proportional.models.ProportionalCalculationItem;
import java.util.List;
import org.javatuples.Pair;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Update Team with proportional calculation result.
 *
 * @author Rommel Medina
 *
 */
@Component
public class ProportionalResultMapper implements
    Converter<Pair<TeamDto, List<ProportionalCalculationItem>>, TeamDto> {
  /**
   * Fill the TeamDTO request with ProportionalCalculaitons results.
   *
   * @param requestData Original TeamDto and results from proportional calculation data.
   * @return TeamDTO
   */
  @Override
  public TeamDto convert(Pair<TeamDto, List<ProportionalCalculationItem>> requestData) {
    TeamDto request = requestData.getValue0();
    List<ProportionalCalculationItem> results = requestData.getValue1();
    for (PlayerDto findedPlayer : request.getPlayers()) {
      ProportionalCalculationItem pci = results.stream()
          .filter(player -> findedPlayer.getId().equals(player.getId()))
          .findAny()
          .orElse(null);
      
      if (pci != null) {
        findedPlayer.setFullSalary(pci.getResult());
      }
    }
    return request;
  }
}
