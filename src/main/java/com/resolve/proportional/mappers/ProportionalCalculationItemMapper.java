/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.mappers;

import com.resolve.football.models.PlayerDto;
import com.resolve.proportional.models.ProportionalCalculationItem;
import com.resolve.salary.calculation.models.SalaryConstants;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Convert Player to ProportionalCalculationItem.
 *
 * @author Rommel Medina
 *
 */
@Component
public class ProportionalCalculationItemMapper implements
    Converter<PlayerDto, ProportionalCalculationItem> {

  @Override
  public ProportionalCalculationItem convert(PlayerDto player) {
    ProportionalCalculationItem item = new ProportionalCalculationItem();
    item.setId(player.getId());
    item.setFixedBase(player.getFixedSalary());
    item.setBonus(player.getBonus());
    item.addCategory(SalaryConstants.CATEGORY_NAME_LEVEL, player.getLevel().trim().toLowerCase());
    item.addProductionQuantity(SalaryConstants.CATEGORY_NAME_LEVEL, player.getScoredPoints());
    
    return item;
  }
}
