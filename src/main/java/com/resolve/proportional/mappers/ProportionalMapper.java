/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */

package com.resolve.proportional.mappers;

import com.resolve.football.models.PlayerDto;
import com.resolve.football.models.TeamDto;
import com.resolve.proportional.models.ProportionalCalculationItem;
import com.resolve.salary.calculation.models.SalaryConstants;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Convert TeamDTO into Proportional calculation items list.
 *
 * @author rommelmg
 *
 */
@Component
public class ProportionalMapper implements
    Converter<TeamDto, List<ProportionalCalculationItem>> {
  /**
   * Specific mapper.
   */
  @Autowired
  private Converter<PlayerDto, ProportionalCalculationItem> mapperService;
  
  /**
   * Mapper constructor.
   *
   * @param itemMapper Proportional calculation item.
   */
  public ProportionalMapper(Converter<PlayerDto, ProportionalCalculationItem> itemMapper) {
    this.mapperService = itemMapper;
  }
  
  /**
   * Convert TeamDTO into Proportional calculation items list.
   *
   * @param request Team data.
   * @return list Proportional calculation item's list.
   */
  @Override
  public List<ProportionalCalculationItem> convert(TeamDto request) {
    List<ProportionalCalculationItem> items = new ArrayList<ProportionalCalculationItem>();
    
    for (PlayerDto findedPlayer : request.getPlayers()) {
      ProportionalCalculationItem pci = this.mapperService.convert(findedPlayer);
      
      if (pci != null) {
        pci.addCategory(
            SalaryConstants.CATEGORY_NAME_GROUP,
            request.getName().trim().toLowerCase());
        pci.getProductionQuantity().put(
            SalaryConstants.CATEGORY_NAME_GROUP,
            request.getProduction());
        items.add(pci);
      }
    }
    return items;
  }
}
